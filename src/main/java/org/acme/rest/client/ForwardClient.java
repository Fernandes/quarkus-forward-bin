package org.acme.rest.client;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import org.acme.rest.dto.ResponseDTO;
import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;

@Path("/")
@RegisterRestClient
public interface ForwardClient {

    @GET
    @Produces("application/json")
    List<ResponseDTO> getForward();

}
