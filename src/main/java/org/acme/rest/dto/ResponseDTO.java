package org.acme.rest.dto;

import java.time.LocalDateTime;

import lombok.Builder;
import lombok.Getter;
import lombok.ToString;

@Getter
@Builder
@ToString
public class ResponseDTO {

	private final LocalDateTime created = LocalDateTime.now();
	private String name;
	private String forwardTo;
}
