package org.acme;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.acme.rest.client.ForwardClient;
import org.acme.rest.dto.ResponseDTO;
import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.eclipse.microprofile.metrics.MetricUnits;
import org.eclipse.microprofile.metrics.annotation.Counted;
import org.eclipse.microprofile.metrics.annotation.Timed;
import org.eclipse.microprofile.rest.client.inject.RestClient;

import lombok.extern.jbosslog.JBossLog;

@Path("/")
@JBossLog
public class SimpleResource {

	@ConfigProperty(name = "quarkus.application.name")
	String appName;
	
	@ConfigProperty(name = "org.acme.rest.client.ForwardClient/mp-rest/url")
	String forwardTo;
	
	@Inject
	@RestClient
	ForwardClient forwardClient;
	
	@GET
    @Path("/forward")
    @Produces(MediaType.APPLICATION_JSON)
	@Counted(name = "performedForward", description = "How many forwards have been performed.")
    @Timed(name = "forwardTimer", description = "A measure how long it takes to perform the forward.", unit = MetricUnits.MILLISECONDS)
    public List<ResponseDTO> forward() {
		log.debugv("forwarding...");

		List<ResponseDTO> ret = new ArrayList<>();
		
		ret.add(ResponseDTO.builder()
        		.name(appName)
        		.forwardTo(forwardTo)
        		.build());
		
		ret.addAll(forwardClient.getForward());
		
		log.debugv("forwarding returning... {0}", ret);
		
		return ret;
    }
	
	@GET
    @Path("/")
    @Produces(MediaType.APPLICATION_JSON)
    public List<ResponseDTO> get() {
		
		List<ResponseDTO> ret = Arrays.asList(ResponseDTO.builder()
        		.name(appName + " - END").build());
		
		log.debugv("get() {0}", ret);
        return ret;
    }
	
    @GET
    @Path("/{name}")
    @Produces(MediaType.TEXT_PLAIN)
    public String getName(@PathParam("name") String name) {
        return "Hello " + name;
    }
}